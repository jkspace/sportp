import unittest
from sport.bhb import *


def activity(activity_type='Run'):
    return {
        'start_date_local': '2021-07-21T14:12:05Z',
        'name': 'My fun run',
        'distance': 1000.0,
        'moving_time': 6543,
        'type': activity_type,
        'extra_fields': 'extra_values'
    }


class TestBhb(unittest.TestCase):

    def test_converted_fields(self):
        bhb = to_bhb_format(activity())

        self.assertEqual('2021-07-21', bhb['Activity Date'])
        self.assertEqual('My fun run', bhb['Comment'])
        self.assertEqual('01:49:03', bhb['Activity Time'])
        self.assertEqual('Run', bhb['Activity Type'])
        self.assertEqual('0.62', bhb['Distance in Miles'])

    def test_activity_type(self):
        def bhb(strava_activity):
            return to_bhb_format(activity(strava_activity))

        def activity_type(bhb_format):
            return bhb_format['Activity Type']

        other = 'Other - Any Activity That Covers Distance'
        run = bhb('Run')
        walk = bhb('Walk')
        ride = bhb('Ride')
        ski = bhb('Ski')
        nordic_ski = bhb('NordicSki')
        inline_skate = bhb('InlineSkate')
        roller_ski = bhb('RollerSki')
        hike = bhb('Hike')
        swim = bhb('Swim')

        self.assertEqual('Run', activity_type(run))
        self.assertEqual('Walk', activity_type(walk))
        self.assertEqual('Ride', activity_type(ride))
        self.assertEqual('Ski', activity_type(ski))
        self.assertEqual('Ski', activity_type(nordic_ski))
        self.assertEqual(other, activity_type(inline_skate))
        self.assertEqual(other, activity_type(roller_ski))
        self.assertEqual('Walk', activity_type(hike))
        self.assertEqual('Swim', activity_type(swim))

if __name__ == '__main__':
    unittest.main()
