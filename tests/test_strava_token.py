import unittest
from datetime import datetime, timedelta
from unittest.mock import patch

from sport.StravaToken import StravaToken


def token_data(token: str = 'abcde', expiration: datetime = datetime.now() + timedelta(days=1)):
    day_in_s = 24 * 60 * 60

    return {
        'token_type': 'Bearer',
        'access_token': token,
        'expires_at': expiration.timestamp(),
        'expires_in': day_in_s,
        'refresh_token': 'zyxwv'
    }


_id = 515
_secret = 'client_secret'


class TestStravaToken(unittest.TestCase):

    def test_access_token(self):
        token = StravaToken(_id, _secret, token_data())
        self.assertEqual('abcde', token.access_token())

    def test_token_data(self):
        current_token_data = token_data()
        token = StravaToken(_id, _secret, token_data())
        self.assertEqual(current_token_data, token.data())

    def test_refresh_near_expiring_token(self):
        expiration = datetime.now() + timedelta(minutes=15)
        token = StravaToken(_id, _secret, token_data(expiration=expiration))

        new_token = "fresh is best"
        new_token_data = token_data(token=new_token)

        def mock_refresh(*args, **kwargs):
            if args == (_id, _secret, 'zyxwv'):
                return new_token_data
            else:
                return None

        with patch('sport.strava.current_token') as mock:
            mock.side_effect = mock_refresh
            self.assertEqual(new_token_data, token.data())
            self.assertEqual(new_token, token.access_token())


if __name__ == '__main__':
    unittest.main()
