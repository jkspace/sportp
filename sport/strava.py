from typing import Any

import requests


def list_activities(token: str, after: int) -> list[dict[str, Any]]:
    authorization = {'Authorization': f'Bearer {token}'}
    request_parameters = {'after': after}

    response = requests.get('https://www.strava.com/api/v3/athlete/activities',
                            headers=authorization, params=request_parameters)

    return response.json()


def current_token(client_id: int, client_secret: str, refresh_token: str) -> dict[str, Any]:
    refresh_parameters = {
        'client_id': client_id,
        'client_secret': client_secret,
        'refresh_token': refresh_token,
        'grant_type': 'refresh_token'
    }

    response = requests.post("https://www.strava.com/api/v3/oauth/token", data=refresh_parameters)

    return response.json()
