import csv
import sys
from datetime import date, datetime, time

from sport import strava
from sport.bhb import to_bhb_format, bhb_fields


def export_to_bhb_format(token: str, start_date: date):
    start_time_stamp = int(datetime.combine(start_date, time(0, 0, 0)).timestamp())
    activities = strava.list_activities(token, start_time_stamp)

    writer = csv.DictWriter(sys.stdout, bhb_fields, extrasaction='ignore')

    bhb = [to_bhb_format(a) for a in activities if a['distance'] > 0.0]

    writer.writeheader()
    writer.writerows(bhb)
