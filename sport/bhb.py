from typing import Any


def to_bhb_format(strava_format: dict[str, Any]) -> dict[str, str]:
    bhb = _Bhb(strava_format)

    return bhb.format()


bhb_fields: list[str] = ['Activity Date', 'Comment', 'Activity Time', 'Activity Type', 'Distance in Miles']


class _Bhb:

    def __init__(self, strava_format):
        self.strava_format = strava_format

    _miles_in_m = 1609.344

    _other_type = 'Other - Any Activity That Covers Distance'
    _bhb_type = {
        'Run': 'Run',
        'Walk': 'Walk',
        'Ride': 'Ride',
        'Ski': 'Ski',
        'NordicSki': 'Ski',
        'Hike': 'Walk',
        'Swim': 'Swim',

    }

    def _identity(self, bhb_field: str, strava_field: str) -> tuple[str, str]:
        return bhb_field, self.strava_format[strava_field]

    def _activity_date(self) -> tuple[str, str]:
        date = self.strava_format['start_date_local'].split('T')[0]
        return 'Activity Date', date

    def _comment(self) -> tuple[str, str]:
        return self._identity('Comment', 'name')

    def _activity_time(self) -> tuple[str, str]:
        total_seconds = self.strava_format['moving_time']
        (hours, remaining_seconds) = divmod(total_seconds, 3600)
        (minutes, seconds) = divmod(remaining_seconds, 60)

        return 'Activity Time', f'{hours:02}:{minutes:02}:{seconds:02}'

    def _activity_type(self) -> tuple[str, str]:
        return 'Activity Type', self._bhb_type.get(self.strava_format['type'], self._other_type)

    def _distance_miles(self) -> tuple[str, str]:
        miles = self.strava_format['distance'] / self._miles_in_m
        return 'Distance in Miles', f'{miles:.2f}'

    _transforms = [_activity_date, _comment, _activity_time, _activity_type, _distance_miles]

    def format(self) -> dict[str, str]:
        values = [t(self) for t in self._transforms]
        return dict(values)
