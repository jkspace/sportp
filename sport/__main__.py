#!/usr/local/bin/python3
import sys
from datetime import date
from pathlib import Path

from sport.StravaToken import strava_token
from sport.sporter import export_to_bhb_format

start_date = date.fromisoformat(sys.argv[1])

config_path = Path.home().joinpath('.sport')

with strava_token(config_path) as t:
    export_to_bhb_format(t, start_date)


