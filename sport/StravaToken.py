import json
from contextlib import contextmanager
from datetime import datetime, timedelta
from pathlib import Path
from typing import Any

from sport import strava


@contextmanager
def strava_token(config_path: Path):
    secret_file = config_path.joinpath('api')
    token_file = config_path.joinpath('token')
    with secret_file.open() as s, token_file.open() as t:
        api = json.load(s)
        token_data = json.load(t)
        token = StravaToken(api['client_id'], api['client_secret'], token_data)

        try:
            yield token.access_token()
        finally:
            new_token_data = json.dumps(token.data())
            token_file.write_text(new_token_data)


class StravaToken:
    _expiration_margin = timedelta(minutes=30)

    def __init__(self, client_id: int, client_secret: str, token_data: dict[str, Any]):
        self._client_id = client_id
        self._client_secret = client_secret
        self._set_token_data(token_data)

    def access_token(self) -> str:
        self._refresh()
        return self.token_data['access_token']

    def data(self) -> dict[str, Any]:
        self._refresh()
        return self.token_data

    def _refresh(self):
        now = datetime.now()
        if now >= self.refresh_time:
            self.token_data = strava.current_token(self._client_id, self._client_secret, self.token_data['refresh_token'])

    def _set_token_data(self, token_data):
        self.token_data = token_data
        self.refresh_time = datetime.fromtimestamp(token_data['expires_at']) - self._expiration_margin
