# Instructions

1. `pip install -r requirements.txt`
2. Add client id and secret to `~/.sport/api`, in JSON format:

         { "client_id": 1234, "client_secret":  "abcdef"}

3. Copy the JSON response from Strava, containing the access token, and refresh token into `~/.sport/token`, eg.

         {
           "token_type": "Bearer",
           "access_token": "abcdef",
           "expires_at": 1627804072,
           "expires_in": 21600,
           "refresh_token": "vwxyz"
         }

4. `python -m sport [start_date] > activites.csv`


* The access token must have `scope:read_all`
* `start_date` must be in the format `yyyy-mm-dd`, and will requests activities from midnight of that day.
* `~/.sport` should be set to be readable by owner only, as well as the files it contains. Eg.

        chmod 700 ~/.sport
        chmod 600 ~/.sport/api
        chmod 600 ~/.sport/token

